import './App.css';
import { BrowserRouter as Router} from "react-router-dom";
import Direction from './Route/Direction';
function App() {
  return (
    <Router>
    <Direction/>
  </Router>
  );
}

export default App;
