import React from "react";
import { Route, Switch } from "react-router-dom";
import AdminHomepage from "../components/AdminPage/AdminHomepage";
import Login from "../components/Auth/Login";
import SignUp from "../components/Auth/SignUp";
import BlogDetail from "../components/HomePage/BlogDetail";
import CategoryPost from "../components/HomePage/CategoryPost";
import Footer from "../components/HomePage/Footer";
import HeroBanner from "../components/HomePage/HeroBanner";
import Homepage from "../components/HomePage/Homepage";
import Navbar from "../components/HomePage/Navbar";
import UploadFile from "../components/UploadFile";

function Direction(props) {
  return (
    <div>
      <Switch>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/signup">
          <SignUp />
        </Route>
        <Route exact path="/adminhomepage">
          <AdminHomepage />
        </Route>
        <Route exact path="/upload">
          <UploadFile />
        </Route>
        <Route exact path="/">
          <Homepage />
        </Route>
        <Route exact path="/blogdetails/:slug">
          <Navbar />
          <HeroBanner title="Poo Blog" subTitle="Hạnh phúc hơn mỗi ngày bạn nhé" des="Bie Sukrit"/>
          <BlogDetail/>
          <Footer />
        </Route>
        <Route exact path="/category/:slug">
          <Navbar />
          <HeroBanner title="Poo Blog" subTitle="Lựa chọn của bạn là của riêng bạn nhé!"/>
          <CategoryPost/>
          <Footer />
        </Route>
      </Switch>
    </div>
  );
}

export default Direction;
