import React, { useState } from "react";
import axios from "axios";
function UploadFile() {
  const [file, setFile] = useState(null);

  const onFileChange = (event) => {
    setFile({ ...file, file: event.target.files[0]});
  };
  const onFileUpload = () => {
    const data= file.file;
    console.log(file.file);
    console.log(data);
    console.log(window.location.origin);
    const formData = new FormData();
    formData.append("file",data);
    axios({
      method: 'post',
      url: 'http://localhost:8080/upload',
      data: formData,
      headers:{
        "Content-type":"multipart/form-data",
        "Access-Control-Allow-Origin":"*"
    } 
    });

  };

  const fileData = () => {
    if (file)
      return (
        <div>
          <h2>File Details:</h2>
          <p>File Name: {file.file.name}</p>
          <p>File Type: {file.file.type}</p>
          <p>Last Modified: {file.file.lastModifiedDate.toDateString()}</p>
        </div>
      );
    else
      return (
        <div>
          <br />
          <h4>Choose before Pressing the Upload button</h4>
        </div>
      );
  };

  return (
    <div>
      <div>
        <h1>GeeksforGeeks</h1>
        <h3>File Upload using React!</h3>
        <div>     
            <input
              type="file"
              name="file"
              accept="image/*"
              onChange={(event) => onFileChange(event)}
            />
            <button onClick={() => onFileUpload()}>Upload!</button>
        </div>
        { fileData()}
      </div>
    </div>
  );
}

export default UploadFile;
