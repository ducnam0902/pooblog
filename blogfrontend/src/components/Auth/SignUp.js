import React from 'react'

function SignUp(props) {
    return (
        <div className="container-login100" style={{backgroundImage: 'url("images/bg-01.jpg")'}}>
        <div className="wrap-login100 p-l-55 p-r-55 p-t-45 p-b-20">
        <form className="login100-form validate-form">
            <span className="login100-form-title p-b-37">
              Sign Up
            </span>
            <div className="wrap-input100 validate-input m-b-20">
              <input className="input100" type="text" name="username" placeholder="Username" />
              <span className="focus-input100" />
            </div>
            <div className="wrap-input100 validate-input m-b-25">
              <input className="input100" type="email" name="email" placeholder="Email" />
              <span className="focus-input100" />
            </div>
            <div className="wrap-input100 validate-input m-b-25">
              <input className="input100" type="password" name="password" placeholder="Password" />
              <span className="focus-input100" />
            </div>

            <div className="container-login100-form-btn mb-5">
              <button className="login100-form-btn">
                Sign Up
              </button>
            </div>
            <div className="text-center">
              <a href="/login" className="txt2 hov1">
                Sign In
              </a>
            </div>
          </form>
        </div>
      </div>
    )
}

export default SignUp

