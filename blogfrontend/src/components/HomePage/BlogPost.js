import React from 'react';
import CategoryDataServices from '../services/CategoryServices';
import PostDataServices from '../services/PostServices';
import { useEffect, useState } from "react";
function BlogPost() {
    const [categories, setCategories] = useState([]);
    const [newPost, setNewPost] = useState([]);
    useEffect(() => {
        retrieveCategories();
        retrieveNewestPost();
      }, []);

      const retrieveNewestPost = () => {
        PostDataServices.getNewestPost()
          .then((response)=> {
            setNewPost(response.data);
            
          })
          .catch((e)=> console.log(e));
       }
      const retrieveCategories = () => {
        CategoryDataServices.getAll()
          .then((response) => {
            setCategories(response.data);
          })
          .catch((e) => console.log(e));
      };
      const showCategories = () => {
        if(categories)
          return (
              categories.map((value, key ) => (
                <li key={key}>
                <a href={`/category/${value.slug}`} className="d-flex justify-content-between">
                  <p>{value.name}</p>
                </a>
              </li>
              ))
          )
      }
      const showNewestPost = () => {
        if(newPost)
         return (
           newPost.map((value, key) => (
            <div key={key} className="single-recent-blog-post">
            <div className="thumb">
              <img className="img-fluid" width="720" height="482" src={`http://localhost:8080/images/${value.image}`} alt="" />
            </div>
            <div className="details mt-20">
              <a href={`/blogdetails/${value.slug}`}>
                <h3>{value.title}</h3>
              </a>
              <p>{value.subTitle}</p>
              <a className="button" href={`/blogdetails/${value.slug}`}>Read More <i className="ti-arrow-right" /></a>
            </div>
          </div>
           ))
         )
       }
    return (
        <section className="blog-post-area section-margin mt-4">
  <div className="container">
    <div className="row">
      <div className="col-lg-8">
        {showNewestPost()}
        </div>
      {/* Start Blog Post Siddebar */}
      <div className="col-lg-4 sidebar-widgets">
        <div className="widget-wrap">
          <div className="single-sidebar-widget post-category-widget">
            <h4 className="single-sidebar-widget__title">Catgory</h4>
            <ul className="cat-list mt-20">
                {showCategories()}
             

            </ul>
          </div>
          
        </div>
      </div>
    </div>
    {/* End Blog Post Siddebar */}
  </div>
</section>

    )
}

export default BlogPost
