import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import PostDataService from "../services/PostServices";
import CategoryDataServices from "../services/CategoryServices";
import ReactHtmlParser from 'react-html-parser';
function BlogDetail() {
  let { slug } = useParams();
  const [data, setData] = useState({});
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    retrieveCategories();
  }, []);
  const retrieveCategories = () => {
    CategoryDataServices.getAll()
      .then((response) => {
        setCategories(response.data);
      })
      .catch((e) => console.log(e));
  };
  useEffect(() => {
    PostDataService.getPostDetails(slug)
      .then((response) => {
        console.log(response.data);
        setData(response.data);
      })
      .catch((e) => console.log(e));
  }, []);
  const showCategories = () => {
    if(categories)
      return (
          categories.map((value, key ) => (
            <li key={key}>
            <a href={`/category/${value.slug}`} className="d-flex justify-content-between">
              <p>{value.name}</p>
            </a>
          </li>
          ))
      )
  }
  return (
    <section className="blog-post-area section-margin">
      <div className="container">
        <div className="row">
          <div className="col-lg-8">
            <div className="main_blog_details">
              <img className="img-fluid" src={`http://localhost:8080/images/${data.image}`} alt="" />
                <h4>
                 {data.title}
                </h4>
              <div className="user_details">
              </div>
              <p>
               {data.subTitle}
              </p>
               {ReactHtmlParser(data.descriptions)}
              
              </div>
            </div>
            <div className="col-lg-4 sidebar-widgets">
        <div className="widget-wrap">
          <div className="single-sidebar-widget post-category-widget">
            <h4 className="single-sidebar-widget__title">Catgory</h4>
            <ul className="cat-list mt-20">
                {showCategories()}
             

            </ul>
          </div>
          
        </div>
      </div>
          </div>
        </div>
    </section>
  );
}

export default BlogDetail;
