import React from 'react'

function Footer() {
    return (
        <footer className="footer-area section-padding">
  <div className="container">
    <div className="row">
      <div className="col-lg-5  col-md-6 col-sm-6">
        <div className="single-footer-widget">
          <h6>About Us</h6>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore
            magna aliqua.
          </p>
        </div>
      </div>
      <div className="col-lg-5  col-md-6 col-sm-6">
        <div className="single-footer-widget">
          <h6>Newsletter</h6>
          <p>Stay update with our latest</p>
          <div id="mc_embed_signup">
            <form target="_blank" method="get" className="form-inline">
              <div className="d-flex flex-row">
                <input className="form-control" name="EMAIL" placeholder="Enter Email"  required type="email" />
                <button className="click-btn btn btn-default"><span className="lnr lnr-arrow-right" /></button>
                <div style={{position: 'absolute', left: '-5000px'}}>
                  <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabIndex={-1} defaultValue type="text" />
                </div>
                {/* <div class="col-lg-4 col-md-4">
                  <button class="bb-btn btn"><span class="lnr lnr-arrow-right"></span></button>
                </div>  */}
              </div>
              <div className="info" />
            </form>
          </div>
        </div>
      </div>
      <div className="col-lg-2 col-md-6 col-sm-6">
        <div className="single-footer-widget">
          <h6>Follow Us</h6>
          <p>Let us be social</p>
          <div className="footer-social d-flex align-items-center">
            <a href="/">
              <i className="fab fa-facebook-f" />
            </a>
            <a href="/">
              <i className="fab fa-twitter" />
            </a>
            <a href="/">
              <i className="fab fa-dribbble" />
            </a>
            <a href="/">
              <i className="fab fa-behance" />
            </a>
          </div>
        </div>
      </div>
    </div>
    <div className="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
      <p className="footer-text m-0">{/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}
        Copyright © All rights reserved | This template is made with Poo Blog
        {/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}</p>
    </div>
  </div>
</footer>

    )
}

export default Footer
