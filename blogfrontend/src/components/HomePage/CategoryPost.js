import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import CategoryDataServices from '../services/CategoryServices';
import PostDataServices from '../services/PostServices';
import { useParams } from 'react-router';
function CategoryPost(props) {
    const [categories, setCategories] = useState([]);
    const [post, setPost] = useState([]);
    let {slug} = useParams();

    useEffect(() => {
        retrieveCategories();
        PostDataServices.getPostByCategory(slug)
            .then((response)=> {
                setPost(response.data);
            })
            .catch((e) => console.log(e));
        
    }, []);
    const retrieveCategories = () => {
        CategoryDataServices.getAll()
          .then((response) => {
            setCategories(response.data);
          })
          .catch((e) => console.log(e));
      };
    const showCategories = () => {
        if(categories)
          return (
              categories.map((value, key ) => (
                <li key={key}>
                <a href={`/category/${value.slug}`} className="d-flex justify-content-between">
                  <p>{value.name}</p>
                </a>
              </li>
              ))
          )
      }
      const showPostByCategory = () => {
          if(post)
            return (
                post.map((value,key) => (
                    <div className="col-md-6">
                    <div className="single-recent-blog-post card-view">
              <div className="thumb">
                <img className="card-img rounded-0" src={`http://localhost:8080/images/${value.image}`} alt="" />
                
              </div>
              <div className="details mt-20">
                <a href={`/blogdetails/${value.slug}`}>
                  <h3>{value.title}</h3>
                </a>
                <p>{value.subTitle}</p>
                <a className="button" href={`/blogdetails/${value.slug}`}>Read More <i className="ti-arrow-right" /></a>
              </div>
            </div>
            </div>
                ))
            )
      }
    return (
        <section className="blog-post-area section-margin">
  <div className="container">
    <div className="row">
      <div className="col-lg-8">
        <div className="row">
         
              {showPostByCategory()}
            
          </div>
      </div>
      {/* Start Blog Post Siddebar */}
      <div className="col-lg-4 sidebar-widgets">
        <div className="widget-wrap">
          <div className="single-sidebar-widget post-category-widget">
            <h4 className="single-sidebar-widget__title">Catgory</h4>
            <ul className="cat-list mt-20">
            {showCategories()}
            </ul>
          </div>
         </div>
      </div>
    </div>
    {/* End Blog Post Siddebar */}
  </div>
</section>

    )
}

CategoryPost.propTypes = {

}

export default CategoryPost

