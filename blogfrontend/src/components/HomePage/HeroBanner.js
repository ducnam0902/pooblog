import React from 'react'
import PropTypes from 'prop-types'
function HeroBanner(props) {
    return (
        <section className="mb-30px">
        <div className="container">
          <div className="hero-banner">
            <div className="hero-banner__content">
              <h3>{props.title}</h3>
              <h1>{props.subTitle}</h1>
              <h4>{props.des}</h4>
            </div>
          </div>
        </div>
      </section>
    )
}
HeroBanner.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  des: PropTypes.string,
}
HeroBanner.defaultProps = {
  des: ""
}

export default HeroBanner







