import React from 'react'
import BlogPost from './BlogPost'
import Footer from './Footer'
import HeroBanner from './HeroBanner'
import Navbar from './Navbar'

function Homepage() {
    return (
        <div>
          <Navbar/>
          <HeroBanner title="Poo Blog" subTitle="One More Time With Feeling" des="Nơi mang lại giây phút vui vẻ cho bạn và gia đình" />
          <BlogPost/>
          <Footer/>
        </div>
    )
}

export default Homepage
