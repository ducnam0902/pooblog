import React from "react";
import { useState } from "react";
import AddCategory from "./AddCategory";
import CategoryList from "./CategoryList";
import UpdateCategory from "./UpdateCategory";

function Category() {
  const [addStatus, setAddStatus] = useState(false);
  const [updateStatus, setUpdateStatus] = useState(false);
  const [dataUpdate, setDataUpdate] = useState({});
  const changeAddStatus = () => {
      setAddStatus(!addStatus);
   }
  const showHeader = () => {
      if(!addStatus && !updateStatus)
      return (
        <div>
          <h3 className="text-center mt-3 mb-4">Chuyên mục</h3>
          <div className="row">
            <div className="col-sm-10">
              <form className="form-inline">
                <input
                  className="form-control col-sm-6"
                  type="search"
                  name="keyword"
                  placeholder=" Tìm kiếm"
                  required
                />
                <button
                  type="Submit"
                  className="btn btn-secondary mx-3 rounded"
                >
                  <i className="fas fa-search" />
                  Tìm kiếm
                </button>
              </form>
            </div>
            <div className="col-sm-2 text-right">
              <button className="btn btn-primary" onClick={() => changeAddStatus()}>
                <i className="fas fa-plus" />
                Thêm mới
              </button>
            </div>
          </div>
        </div>
      );
  };
  const showCategoryList = () => {
      if(!addStatus && !updateStatus)
      return (
        <div className="row">
        <table className=" mt-3 table table-hover bang">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Tên</th>
              <th scope="col">Đường dẫn</th>
              <th scope="col">Hành động</th>
            </tr>
          </thead>
          <tbody>
            <CategoryList dataUpdate={(data) => getDataModified(data)}/>
          </tbody>
        </table>
      </div>
      );
  }
  const showCategoryForm = () => {
      if(addStatus)
        return (
            <AddCategory addStatus={() => changeAddStatus()}/>
        );
  }
  const getDataModified = (data) => {
    setUpdateStatus(true);
    setDataUpdate({...dataUpdate,
        categoryId: data.categoryId,
        name: data.name,
        slug: data.slug
    });
   }
   const changeUpdateStatus = () => {
       setUpdateStatus(false);
   }
   const showCategoryUpdateForm = () => {
       if(updateStatus)
        return (<UpdateCategory data={dataUpdate} updateStatus={() => changeUpdateStatus() }/>);
    }
  return <div className="container">
      {showHeader()}
      {showCategoryList()}
      {showCategoryForm()}
      {showCategoryUpdateForm()}
      </div>;
}

export default Category;
