import React from "react";
import PropTypes from "prop-types";
import { useState } from "react";
import { withRouter } from 'react-router-dom';
import CategoryDataServices from "../services/CategoryServices";
function UpdateCategory(props) {
  const [dataUpdate, setDataUpdate] = useState(props.data);

  const handleSubmit = (event) => {
    console.log(dataUpdate);
    CategoryDataServices.update(dataUpdate.categoryId, dataUpdate)
      .then((response) => {
        console.log(response.data);
      })
      .catch((e) => console.log(e));
    window.alert("Bạn đã cập nhật thành công");
    
  };
  const handleCancel = (event) => {
    event.preventDefault();
    props.updateStatus(false);
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setDataUpdate({ ...dataUpdate, [name]: value });
  };
  return (
    <div>
      <h3 className=" mt-3 text-center">Chỉnh sửa chuyên mục</h3>
      <div className="row">
        <div className="col"></div>
        <div className="col-8">
          <form>
            <div className="form-group">
              <h4>Mã chuyên mục</h4>
              <input
                type="text"
                name="categoryId"
                className="form-control"
                readOnly
                defaultValue={props.data.categoryId}
              />
              <h4>Tên chuyên mục</h4>
              <input
                type="text"
                name="name"
                className="form-control"
                defaultValue={props.data.name}
                onChange={(event) => handleInputChange(event)}
              />
              <h4>Đường dẫn</h4>
              <input
                type="text"
                name="slug"
                className="form-control"
                defaultValue={props.data.slug}
                onChange={(event) => handleInputChange(event)}
              />
              <div className="d-flex flex-direction-row justify-content-center">
                <button
                  className="btn btn-primary d-block mt-4 mr-2"
                  onClick={(event) => handleSubmit(event)}
                >
                  Cập nhật
                </button>
                <button
                  className="btn btn-secondary d-block mt-4"
                  onClick={(event) => handleCancel(event)}
                >
                  Huỷ
                </button>
              </div>
            </div>
          </form>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}

UpdateCategory.propTypes = {
  data: PropTypes.object.isRequired,
  updateStatus: PropTypes.func.isRequired,
};

export default withRouter(UpdateCategory);



