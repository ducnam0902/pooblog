import React from 'react'
import { useState } from 'react';
import PropTypes from "prop-types";
import CategoryDataServices from "../services/CategoryServices";
function AddCategory(props) {
    const [category, setCategory] = useState({});
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setCategory({ ...category, [name]: value });
      };
      const AddNewCategory = (event) => {
        event.preventDefault();
        CategoryDataServices.create(category)
            .then(response => {
                    console.log(response.data);
                    props.addStatus();
                }
            )
            .catch(e => console.log(e));
      };
      const cancel = (event) => {
        event.preventDefault();
        props.addStatus();
      }
    return (
        <div className="container">
        <h3 className=" mt-3 text-center">Thêm mới chuyên mục</h3>
        <div className="row">
          <div className="col"></div>
          <div className="col-8">
            <form>
              <div className="form-group">
                <h4>Tên chuyên mục</h4>
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  placeholder="Nhập tên chuyên mục"
                  onChange={handleInputChange}
                  aria-describedby="helpId"
                />
                <h4>Đường dẫn</h4>
                <input
                  type="text"
                  name="slug"
                  className="form-control"
                  placeholder="Nhập đường dẫn"
                  aria-describedby="helpId"
                  onChange={handleInputChange}
                />
                <div className="d-flex flex-direction-row justify-content-center">
                  <button
                    className="btn btn-primary d-block mt-4 mr-2"
                    onClick={(event) => AddNewCategory(event)}
                  >
                    Thêm mới
                  </button>
                  <button className="btn btn-secondary d-block mt-4" onClick={(event) => cancel(event)}>Huỷ</button>
                </div>
              </div>
            </form>
          </div>
          <div className="col"></div>
        </div>
      </div>
    )
}
AddCategory.propTypes = {
    addStatus: PropTypes.func.isRequired,
  };

export default AddCategory
