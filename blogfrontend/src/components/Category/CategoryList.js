import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import CategoryDataServices from "../services/CategoryServices";
import PropTypes from "prop-types";
function CategoryList(props) {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    retrieveCategories();
  }, []);
  const retrieveCategories = () => {
    CategoryDataServices.getAll()
      .then((response) => {
        setCategories(response.data);
      })
      .catch((e) => console.log(e));
  };
  const editCategory = (value) => {
    props.dataUpdate(value);
  };
  const deleteCategory = (value) => {
    const deleteStatus = window.confirm(
      "Bạn chắc chắn muốn xóa " + value.name + "?"
    );

    if (deleteStatus) {
      CategoryDataServices.remove(value.categoryId)
        .then((response) => {
          retrieveCategories();
        })
        .catch((e) => console.log(e));
    }
  };
  const showCategoriesData = () => {
    if (categories)
      return categories.map((value, key) => (
        <tr key={key}>
          <td>{key + 1}</td>
          <td>{value.name}</td>
          <td>{value.slug}</td>
          <td>
            <button
              className="btn btn-warning mr-3"
              onClick={() => editCategory(value)}
            >
              Sửa
            </button>
            <button
              className="btn btn-danger"
              onClick={() => deleteCategory(value)}
            >
              Xóa
            </button>
          </td>
        </tr>
      ));
    else return null;
  };
  return showCategoriesData();
}
CategoryList.propTypes = {
    dataUpdate: PropTypes.func.isRequired,
  };

export default CategoryList;
