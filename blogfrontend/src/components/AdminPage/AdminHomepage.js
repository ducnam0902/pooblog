import React, { useState } from "react";
import Category from "../Category/Category";
import Post from "../Post/Post";
function AdminHomepage(props) {
  const [isShowSideNav, setIsShowSideNav] = useState(true);
  const [onCategory, setOnCategory] = useState(false);
  const [onPost, setOnPost] = useState(false);
  const isCategory = () => {
    setOnCategory(true);
    setOnPost(false);
  };
  
  const isPost = () => {
    setOnPost(true);
    setOnCategory(false);
  }

  const showSideNav = () => {
    setIsShowSideNav(!isShowSideNav);
  };

  return (
    <div className={isShowSideNav ? "sb-nav-fixed" : "sb-sidenav-toggled"}>
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a className="navbar-brand d-inline" href="/">
          Poo Blog
        </a>
        <button
          className="btn btn-link btn-sm order-1 order-lg-0"
          id="sidebarToggle"
          onClick={() => showSideNav()}
        >
          <i className="fas fa-bars" />
        </button>
        {/* Navbar Search*/}
        {/* Navbar*/}
        <ul className="navbar-nav ml-sm-auto">
          <li className="nav-item dropdown">
            <a
              className="nav-link dropdown-toggle"
              id="userDropdown"
              href="/"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <i className="fas fa-user fa-fw" />
            </a>
            <div
              className="dropdown-menu dropdown-menu-right"
              aria-labelledby="userDropdown"
            >
              <a className="dropdown-item" href="login.html">
                Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
          <nav
            className="sb-sidenav accordion sb-sidenav-dark"
            id="sidenavAccordion"
          >
            <div className="sb-sidenav-menu">
              <div className="nav">
                <div className="sb-sidenav-menu-heading">Core</div>
                <a className="nav-link" href="/adminhomepage">
                  <div className="sb-nav-link-icon">
                    <i className="fas fa-tachometer-alt" />
                  </div>
                  Dashboard
                </a>
                <div className="sb-sidenav-menu-heading">Interface</div>
                <button
                  className="nav-link collapsed cursor"
                  onClick={() => isCategory()}
                  data-toggle="collapse"
                  data-target="#collapseLayouts"
                  aria-expanded="false"
                  aria-controls="collapseLayouts"
                >
                  <div className="sb-nav-link-icon">
                    <i className="fas fa-columns" />
                  </div>
                  Categories
                  <div className="sb-sidenav-collapse-arrow">
                    <i className="fas fa-angle-down" />
                  </div>
                </button>
                <button
                  className="nav-link collapsed cursor"
                  onClick={() => isPost()}
                  data-toggle="collapse"
                  data-target="#collapsePages"
                  aria-expanded="false"
                  aria-controls="collapsePages"
                >
                  <div className="sb-nav-link-icon">
                    <i className="fas fa-book-open" />
                  </div>
                  Posts
                  <div className="sb-sidenav-collapse-arrow">
                    <i className="fas fa-angle-down" />
                  </div>
                </button>
              </div>
              <div className="sb-sidenav-footer fixed-bottom">
                <div className="small">Logged in as:</div>
                Start Bootstrap
              </div>
            </div>
          </nav>
        </div>
        <div id="layoutSidenav_content">
          {onCategory && <Category/> }
          {onPost && <Post/>}
        </div>
      </div>
    </div>
  );
}

export default AdminHomepage;
