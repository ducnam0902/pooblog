import React from 'react'
import { useState } from 'react';
import AddPost from './AddPost';
import PostList from './PostList';
import UpdatePost from './UpdatePost';

function Post() {
    const [addStatus, setAddStatus] = useState(false);
    const [updateStatus, setUpdateStatus] = useState(false);
    const [dataUpdate, setDataUpdate] = useState({});
    const changeAddStatus = () => {
        setAddStatus(!addStatus);
     }
     const showHeader = () => {
        if(!addStatus && !updateStatus)
        return (
          <div>
            <h3 className="text-center mt-3 mb-4">Bài viết</h3>
            <div className="row">
              <div className="col-sm-10">
                <form className="form-inline">
                  <input
                    className="form-control col-sm-6"
                    type="search"
                    name="keyword"
                    placeholder=" Tìm kiếm"
                    required
                  />
                  <button
                    type="Submit"
                    className="btn btn-secondary mx-3 rounded"
                  >
                    <i className="fas fa-search" />
                    Tìm kiếm
                  </button>
                </form>
              </div>
              <div className="col-sm-2 text-right">
                <button className="btn btn-primary" onClick={() => changeAddStatus()}>
                  <i className="fas fa-plus" />
                  Thêm mới
                </button>
              </div>
            </div>
          </div>
        );
    };
    
    const showPostList = () => {
        if(!addStatus && !updateStatus)
      return (
        <div className="row">
        <table className=" mt-3 table table-hover bang">
          <thead>
            <tr>
            <th scope="col">#</th>
                <th scope="col">Tiêu đề bài đăng</th>
                <th scope="col">Tiêu đề phụ</th>
                <th scope="col">Ảnh</th>
                <th scope="col">Nội dung</th>
                <th scope="col">Đường dẫn</th>
                <th scope="col">Chuyên đề</th>
                <th scope="col">Hành động</th>
            </tr>
          </thead>
          <tbody>
            <PostList dataUpdate={(data) => getDataModified(data)}/>
          </tbody>
        </table>
      </div>
      );
     }
     const showPostForm = () => {
        if(addStatus)
          return (
              <AddPost addStatus={() => changeAddStatus()}/>
          );
    }
     const getDataModified = (data) => {
        setUpdateStatus(true);
        setDataUpdate({...dataUpdate,
          title:data.title,
          subTitle:data.subTitle,
          slug: data.slug,
          postId:data.postId,
          image:data.image,
          descriptions: data.descriptions,
          category: data.category
            
        });
       }
       const changeUpdateStatus = () => {
        setUpdateStatus(false);
    }
    const showPostUpdateForm = () => {
        if(updateStatus)
         return (<UpdatePost data={dataUpdate} updateStatus={() => changeUpdateStatus() }/>);
     }
    return<div className="container">
    {showHeader()}
    {showPostList()}
    {showPostForm()}
    {showPostUpdateForm()}
    </div>;
}

export default Post
