import React from "react";
import PropTypes from "prop-types";
import { useState } from "react";
import { withRouter } from "react-router-dom";
import CategoryDataServices from "../services/CategoryServices";
import PostDataServices from "../services/PostServices";
import CKEditor from "ckeditor4-react";
import axios from "axios";
import { useEffect } from "react";
function UpdatePost(props) {
  const [dataUpdate, setDataUpdate] = useState(props.data);
  const [categories, setCategories] = useState([]);
  const [file, setFile] = useState(null);
  useEffect(() => {
    retrieveCategories();
  }, []);

  const retrieveCategories = () => {
    CategoryDataServices.getAll()
      .then((response) => {
        setCategories(response.data);
      })
      .catch((e) => console.log(e));
  };
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === "image") {
      if (value) {
        if (event.target.files[0].size >= 1048576)
          window.alert("Vui lòng chọn ảnh dưới 1 MB");
        else {
          setFile({ ...file, file: event.target.files[0] });
          setDataUpdate({ ...dataUpdate, image: event.target.files[0].name });
        }
      }
    } else if (name === "category") {
      CategoryDataServices.get(value)
        .then((response) => {
            setDataUpdate({ ...dataUpdate, [name]: response.data });
        })
        .catch((e) => console.log(e));
    } else setDataUpdate({ ...dataUpdate, [name]: value });
  };
  const handleEditorChange = (event) => {
    setDataUpdate({ ...dataUpdate, descriptions: event.editor.getData() });
  };
  const handleSubmit = (event) => {
     PostDataServices.update(dataUpdate.postId,dataUpdate)
     .then((response) => {
        console.log(response.data);
      })
      .catch((e) => console.log(e));
      if(file){
        const data = file.file;
        const formData = new FormData();
        formData.append("file", data);
        axios({
          method: "post",
          url: "http://localhost:8080/upload",
          data: formData,
          headers: {
            "Content-type": "multipart/form-data",
            "Access-Control-Allow-Origin": "*",
          },
        });
      }
    window.alert("Bạn đã cập nhật thành công");
   }

  const handleCancel = (event) => {
    event.preventDefault();
    props.updateStatus(false);
  };
  return <div className="container">
      <h3 className=" mt-3 text-center">Đăng bài</h3>
      <div className="row">
        <div className="col"></div>
        <div className="col-8">
          <form>
            <div className="form-group">
            <h4>Mã bài đăng</h4>
              <input
                type="text"
                name="postId"
                className="form-control"
                placeholder=""
                onChange={handleInputChange}
                aria-describedby="helpId"
                value={props.data.postId}
              />
              <h4>Tiêu đề bài đăng</h4>
              <input
                type="text"
                name="title"
                className="form-control"
                placeholder=""
                onChange={handleInputChange}
                aria-describedby="helpId"
                value={props.data.title}
              />
              <h4>Tiêu đề phụ</h4>
              <input
                type="text"
                name="subTitle"
                className="form-control"
                placeholder=""
                aria-describedby="helpId"
                onChange={handleInputChange}
                value={props.data.subTitle}
              />
              <h4>Ảnh</h4>
              <input
                type="file"
                name="image"
                className="form-control"
                placeholder=""
                aria-describedby="helpId"
                onChange={handleInputChange}
               
                accept="image/png, image/jpeg"
              />
              <h4>Nội dung</h4>
              <CKEditor
              data={props.data.descriptions}
              onChange={(event) => handleEditorChange(event)} />
              <h4>Đường dẫn</h4>
              <input
                type="text"
                name="slug"
                className="form-control"
                placeholder=""
                aria-describedby="helpId"
                onChange={handleInputChange}
                value={props.data.slug}
              />
              <h4>Chuyên đề</h4>
              <select
                className="form-control"
                name="category"
                onChange={handleInputChange}
                value={props.data.category.categoryId}
              >
                <option>Chọn chuyên đề</option>
                {categories.map((value, key) => (
                  <option key={key} value={value.categoryId}>
                    {value.name}
                  </option>
                ))}
              </select>
              <div className="d-flex flex-direction-row justify-content-center">
                <button
                  className="btn btn-primary d-block mt-4 mr-2"
                  onClick={(event) => handleSubmit(event)}
                >
                  Cập nhật
                </button>
                <button
                  className="btn btn-secondary d-block mt-4"
                  onClick={(event) => handleCancel(event)}
                >
                  Huỷ
                </button>
              </div>
            </div>
          </form>
        </div>
        <div className="col"></div>
      </div>
    </div>;
}

UpdatePost.propTypes = {
  data: PropTypes.object.isRequired,
  updateStatus: PropTypes.func.isRequired,
};

export default UpdatePost;
