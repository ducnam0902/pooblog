import React from "react";
import PropTypes from "prop-types";
import PostDataServices from "../services/PostServices";
import CategoryDataServices from "../services/CategoryServices";
import { useState, useEffect } from "react";
import CKEditor from "ckeditor4-react";
import axios from "axios";
function AddPost(props) {
  const [post, setPost] = useState({});
  const [categories, setCategories] = useState([]);
  const [file, setFile] = useState(null);
  useEffect(() => {
    retrieveCategories();
  }, []);

  const retrieveCategories = () => {
    CategoryDataServices.getAll()
      .then((response) => {
        setCategories(response.data);
      })
      .catch((e) => console.log(e));
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === "image") {
      if (value) {
        if (event.target.files[0].size >= 1048576)
          window.alert("Vui lòng chọn ảnh dưới 1 MB");
        else {
          setFile({ ...file, file: event.target.files[0] });
          setPost({ ...post, image: event.target.files[0].name });
        }
      }
    } else if (name === "category") {
      CategoryDataServices.get(value)
        .then((response) => {
          setPost({ ...post, [name]: response.data });
        })
        .catch((e) => console.log(e));
    } else setPost({ ...post, [name]: value });
  };
  const handleEditorChange = (event) => {
    setPost({ ...post, descriptions: event.editor.getData() });
  };

  const AddNewCategory = (event) => {
    event.preventDefault();
    const data = file.file;
    const formData = new FormData();
    formData.append("file", data);

    axios({
      method: "post",
      url: "http://localhost:8080/upload",
      data: formData,
      headers: {
        "Content-type": "multipart/form-data",
        "Access-Control-Allow-Origin": "*",
      },
    });
    PostDataServices.create(post)
      .then((response) => {
        props.addStatus();
      })
      .catch((e) => console.log(e));
  };
  const cancel = (event) => {
    event.preventDefault();
    props.addStatus();
  };
  return (
    <div className="container">
      <h3 className=" mt-3 text-center">Đăng bài</h3>
      <div className="row">
        <div className="col"></div>
        <div className="col-8">
          <form>
            <div className="form-group">
              <h4>Tiêu đề bài đăng</h4>
              <input
                type="text"
                name="title"
                className="form-control"
                placeholder=""
                onChange={handleInputChange}
                aria-describedby="helpId"
              />
              <h4>Tiêu đề phụ</h4>
              <input
                type="text"
                name="subTitle"
                className="form-control"
                placeholder=""
                aria-describedby="helpId"
                onChange={handleInputChange}
              />
              <h4>Ảnh</h4>
              <input
                type="file"
                name="image"
                className="form-control"
                placeholder=""
                aria-describedby="helpId"
                onChange={handleInputChange}
                accept="image/png, image/jpeg"
              />
              <h4>Nội dung</h4>
              <CKEditor onChange={(event) => handleEditorChange(event)} />
              <h4>Đường dẫn</h4>
              <input
                type="text"
                name="slug"
                className="form-control"
                placeholder=""
                aria-describedby="helpId"
                onChange={handleInputChange}
              />
              <h4>Chuyên đề</h4>
              <select
                className="form-control"
                name="category"
                onChange={handleInputChange}
              >
                <option>Chọn chuyên đề</option>
                {categories.map((value, key) => (
                  <option key={key} value={value.categoryId}>
                    {value.name}
                  </option>
                ))}
              </select>
              <div className="d-flex flex-direction-row justify-content-center">
                <button
                  className="btn btn-primary d-block mt-4 mr-2"
                  onClick={(event) => AddNewCategory(event)}
                >
                  Thêm mới
                </button>
                <button
                  className="btn btn-secondary d-block mt-4"
                  onClick={(event) => cancel(event)}
                >
                  Huỷ
                </button>
              </div>
            </div>
          </form>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}

AddPost.propTypes = {
  addStatus: PropTypes.func.isRequired,
};

export default AddPost;
