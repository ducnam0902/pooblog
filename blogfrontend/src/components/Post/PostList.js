import React from "react";
import PropTypes from "prop-types";
import { useState } from "react";
import { useEffect } from "react";
import PostDataServices from "../services/PostServices";
function PostList(props) {
  const [post, setPost] = useState([]);

  useEffect(() => {
    retrievePosts();
  }, []);

  const retrievePosts = () => {
    PostDataServices.getAll()
      .then((response) => {
        setPost(response.data);
      })
      .catch((e) => console.log(e));
  };

  const editPost = (value) => {
    props.dataUpdate(value);
  };
  const deletePost = (value) => {
    const deleteStatus = window.confirm(
      "Bạn chắc chắn muốn xóa " + value.title + "?"
    );

    if (deleteStatus) {
      PostDataServices.remove(value.postId)
        .then((response) => {
          retrievePosts();
        })
        .catch((e) => console.log(e));
    }
  };


  const showPostsData = () => {
    if (post)
      return post.map((value, key) => (
        <tr key={key}>
          <td>{key + 1}</td>
          <td>{value.title}</td>
          <td>
            <textarea value={value.subTitle} readOnly></textarea>
          </td>
          <td>
            <img
              className="img-fluid imagePost"
              src={`http://localhost:8080/images/${value.image}`}
              alt={`${value.image}`}
            />
          </td>
          <td>
            <textarea value={value.descriptions} readOnly></textarea>
          </td>
          <td>{value.slug}</td>
          <td>{value.category.name}</td>
          <td>
            <button
              className="btn btn-warning mr-3"
              onClick={() => editPost(value)}
            >
              Sửa
            </button>
            <button
              className="btn btn-danger"
              onClick={() => deletePost(value)}
            >
              Xóa
            </button>
          </td>
        </tr>
      ));
    else return null;
  };
  return showPostsData();
}

PostList.propTypes = {
  dataUpdate: PropTypes.func.isRequired,
};

export default PostList;
