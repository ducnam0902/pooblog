import http from "../services/http-common"
const getAll  = () => { return http.get("/post");}

const get = (id) => {return http.get(`/post/${id}`); }

const create = (data) => { return http.post("/post",data); }

const update = (id,data) => { return http.put(`/post/${id}`,data);}

const remove = (id) => {return http.delete(`/post/${id}`); }

const findByTitle  = (title) => {return http.get(`/post?title=${title}`); }

const getNewestPost = () => { return http.get("/post/threepost");}

const getPostDetails = (slug) => {return http.get(`/post/postdetails/${slug}`)}

const getPostByCategory = (slug) => {return http.get(`/post/postcategory/${slug}`);}
export default {
    getAll,
    get,
    create,
    update,
    remove,
    findByTitle,
    getNewestPost,
    getPostDetails,
    getPostByCategory,
}
