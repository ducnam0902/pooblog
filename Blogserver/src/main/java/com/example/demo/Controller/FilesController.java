package com.example.demo.Controller;

import java.io.IOException;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
@Controller
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FilesController{
	@PostMapping("/upload")
	public ResponseEntity<HttpStatus> upload(@RequestParam("file") MultipartFile multipartFile) throws IOException{
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		String uploadDir ="src/main/resources/static/images";
		try {
			FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		}
	}