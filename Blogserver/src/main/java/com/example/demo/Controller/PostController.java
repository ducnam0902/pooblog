package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.JPARepository.CategoryRepository;
import com.example.demo.JPARepository.PostRepository;
import com.example.demo.Model.Post;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/post")
public class PostController {
	@Autowired
	PostRepository postRepository;
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@GetMapping
	public ResponseEntity<List<Post>> getALlPosts(@RequestParam(required = false) String title ){
		try {
			List<Post> posts = new ArrayList<Post>();
			if(title == null)
				postRepository.findAll().forEach(posts::add);	
			else
				postRepository.findByTitleContaining(title).forEach(posts::add);
			if(posts.isEmpty())
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			return new ResponseEntity<List<Post>>(posts,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Post> getPostById( @PathVariable("id") long id){
		Optional<Post> postData = postRepository.findById(id);
		if(postData.isPresent())
			return new ResponseEntity<Post>(postData.get(),HttpStatus.OK);
		else
			return new ResponseEntity<Post>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping
	public ResponseEntity<Post> createPost(@RequestBody Post post){	
		try {
			Post _post = postRepository.save(new Post(post.getTitle(),post.getSubTitle(),post.getDescriptions(),post.getImage(), post.getSlug(), post.getCategory()));
			return new ResponseEntity<Post>(_post,HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<Post>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/{id}")
	public ResponseEntity<Post> updatePost(@PathVariable("id") Long id, @RequestBody Post post){
		Optional<Post> postData = postRepository.findById(id);
		//Optional<Category> categoryData = categoryRepository.findById(post.getCategory().getCategoryId());
		if(postData.isPresent()) {
			
			Post _post =postData.get();
			_post.setTitle(post.getTitle());
			_post.setSubTitle(post.getSubTitle());
			_post.setDescriptions(post.getDescriptions());
			_post.setSlug(post.getSlug());
			_post.setImage(post.getImage());
			_post.setCategory(post.getCategory());
			return new ResponseEntity<Post>(postRepository.save(_post), HttpStatus.OK);
		}
		else
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> deletePost(@PathVariable("id") long id){
		try {
			postRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		catch(Exception e) {
			return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/threepost")
	public ResponseEntity<List<Post>> getTopThreePost(){
		try {
			List<Post> posts = new ArrayList<Post>();
			postRepository.findByTopThree().forEach(posts::add);
			System.out.println(posts);
			if(posts.isEmpty())
				return new ResponseEntity<List<Post>>(HttpStatus.NO_CONTENT);
			return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/postdetails/{slug}")
	public ResponseEntity<Post> getPostBySlug( @PathVariable("slug") String slug){
		Optional<Post> postData = postRepository.findBySlugContaining(slug);
		if(postData.isPresent())
			return new ResponseEntity<>(postData.get(),HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	@GetMapping("/postcategory/{slug}")
	public ResponseEntity<List<Post>> getPostByCategory( @PathVariable("slug") String slug){
		List<Post> postData = postRepository.findByCategorySlug(slug);
		if(postData.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		else
			return new ResponseEntity<>(postData,HttpStatus.OK);
	}
}
