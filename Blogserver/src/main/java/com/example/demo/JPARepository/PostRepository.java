package com.example.demo.JPARepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.Model.Post;

public interface PostRepository extends JpaRepository<Post, Long>{
	List<Post> findByTitleContaining( String title);
	@Query(value ="SELECT * FROM POST ORDER BY POST_ID DESC LIMIT 3",nativeQuery = true)
	public List<Post> findByTopThree();
	Optional<Post> findBySlugContaining(String slug);
	@Query(value ="SELECT * FROM POST p INNER JOIN CATEGORY c ON p.category_id =  c.category_id where c.slug=?1",nativeQuery = true)
	List<Post> findByCategorySlug(String slug);
	
}
