package com.example.demo.JPARepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	List<Category> findByNameContaining(String name);
}
